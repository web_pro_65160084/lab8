import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToMany, ManyToOne } from "typeorm"
import { User } from "./User"
import { Order } from "./Order"
import { Product } from "./Product"

@Entity()
export class OrderItem {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    price: number

    @Column()
    total: number

    @Column()
    qty: number

    @CreateDateColumn()
    created: Date

    @UpdateDateColumn()
    updated: Date

    @ManyToOne(() => Order, ((order) => order.orderItems))
    order: Order

    @ManyToOne(() => User, ((user) => user.orders))
    user: User

    @ManyToOne(() => Product, ((product) => product.orderItems))
    product: Product



}
