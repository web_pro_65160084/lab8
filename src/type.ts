import { AppDataSource } from "./data-source"
import { Role } from "./entity/Role"
import { Type } from "./entity/Type"

AppDataSource.initialize().then(async () => {
    const typeRepository = AppDataSource.getRepository(Type)
    await typeRepository.clear()
    var type = new Role()
    type.id = 1
    type.name = "drink"
    await typeRepository.save(type)

    var type = new Role()
    type.id = 2
    type.name = "bakery"
    await typeRepository.save(type)

    var type = new Role()
    type.id = 3
    type.name = "food"
    await typeRepository.save(type)

    const types = await typeRepository.find()
    console.log(types)
}).catch(error => console.log(error))
